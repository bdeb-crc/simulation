#!/usr/bin/env python3
import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth

print("Loading")
data = np.genfromtxt("serialized.csv", delimiter=",", skip_header=1, dtype="float32")
print("Loaded")

# #############################################################################
# Compute clustering with MeanShift

# The following bandwidth can be automatically detected using
bandwidth = estimate_bandwidth(data, quantile=0.5, n_samples=500, n_jobs=-1)
print("bandwidth: ", bandwidth)

ms = MeanShift(bandwidth=bandwidth, bin_seeding=False, n_jobs=-1)
ms.fit(data)
labels = ms.labels_
cluster_centers = ms.cluster_centers_

labels_unique = np.unique(labels)
n_clusters_ = len(labels_unique)

print("number of estimated clusters : %d" % n_clusters_)

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

fig, plots = plt.subplots(3, 2, figsize=(100, 100))

data = data.reshape((-1, 6, 13)).swapaxes(0, 1)

for plot, name, var in zip(plots.flat, ['Nothing', 'Tornado', 'Ball', 'Bumper', 'Special', 'Mushroom'], data):
    # Add a horizontal grid to the plot, but make it very light in color
    # so we can use it for reading data values but not be distracting
    plot.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
    plot.xaxis.grid(True, linestyle='-', which='major', color='lightgrey', alpha=0.5)
    plot.set_title(name)
    plot.set_xlabel('Variable')
    plot.set_ylabel('Importance')
    plot.set_yticklabels(['Tornado', 'Us ball', 'Them ball', 'Us bumper', 'Them bumper', 'Us mushroom', 'Them mushroom', 'Us special', 'Them special', 'Speed', 'Drop speed', 'Side', 'Base'], rotation=45, fontsize=8)
    plot.set_xlim([-10., 10.])

    colors = cycle('grcmyk')
    for k, col in zip(range(n_clusters_), colors):
        my_members = labels == k
        cluster_center = cluster_centers[k]
        plot.boxplot(var[my_members], 0, '', 0)
        # plt.boxplot(cluster_center, 'bo')

plt.savefig('axis.png')
plt.show()
