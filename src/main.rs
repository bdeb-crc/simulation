use structopt::StructOpt;

use self::display::Player;
use self::sim::Genome;

mod display;
mod sim;

/// Simulation for robotique's competition
#[derive(StructOpt, Debug)]
enum Opts {
    /// Run all tests
    #[structopt()]
    Generate,
    /// Interactive simulation, player vs player
    #[structopt()]
    PVP,
    /// Interactive simulation, player vs bot
    #[structopt()]
    PVB,
    /// Interactive simulation, bot vs bot
    #[structopt()]
    BVB,
}

fn load(len: usize) -> Option<Vec<Genome>> {
    let f = std::fs::File::open("serialized.csv").ok()?;
    csv::Reader::from_reader(f)
        .deserialize()
        .take(len)
        .collect::<Result<_, _>>()
        .ok()
}

fn main() {
    let opts = Opts::from_args();
    match opts {
        Opts::Generate => sim::run().unwrap(),
        Opts::PVP => display::run(Player::Human, Player::Human).unwrap(),
        Opts::PVB => {
            if let Some(mut genomes) = load(1) {
                display::run(Player::Human, Player::Bot(genomes.pop().unwrap())).unwrap()
            } else {
                eprintln!("No genome found, please run the simulation before playing with bots.");
                std::process::exit(1)
            }
        }
        Opts::BVB => {
            if let Some(mut genomes) = load(2) {
                display::run(
                    Player::Bot(genomes.pop().unwrap()),
                    Player::Bot(genomes.pop().unwrap()),
                )
                .unwrap()
            } else {
                eprintln!("No genome found, please run the simulation before playing with bots.");
                std::process::exit(1)
            }
        }
    }
}
