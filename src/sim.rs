use pbr::ProgressBar;
use rand::{rngs::ThreadRng, seq::SliceRandom, Rng};
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use sim::{Action, BlackDrop, Game, Side};

#[derive(Debug, Clone, PartialEq, Default, Serialize, Deserialize)]
pub struct Genome([[f32; 13]; 6]);

#[derive(Debug, Clone, PartialEq)]
struct FitnessCalc(Genome);

#[derive(Clone, Debug, PartialEq)]
pub struct TournamentSelector {
    /// The fraction of number of parents to select in relation to the
    /// number of individuals in the population.
    selection_ratio: f64,
    /// The number of individuals per parents.
    num_individuals_per_parents: usize,
    /// The number of participants on each tournament.
    tournament_size: usize,
    /// The probability to pick candidates from one tournament.
    /// Values must be between 0 and 1.0 (inclusive).
    probability: f64,
}

impl TournamentSelector {
    pub fn new(
        selection_ratio: f64,
        num_individuals_per_parents: usize,
        tournament_size: usize,
        probability: f64,
    ) -> Self {
        TournamentSelector {
            selection_ratio,
            num_individuals_per_parents,
            tournament_size,
            probability,
        }
    }
}

impl TournamentSelector {
    fn select_from<'a>(&self, population: &'a [Genome], num_winners: usize) -> Vec<&'a Genome> {
        // select candidates for parents
        population
            .par_chunks(self.tournament_size)
            .flat_map(|tournament| {
                // sort tournament from best performing to worst performing index
                let mut pts: Vec<_> = tournament.iter().map(|i| (0, i)).collect();
                for x in 0..pts.len() {
                    for y in x..pts.len() {
                        let (blue, yellow) = eval(&pts[x].1, &pts[y].1);
                        pts[x].0 += blue;
                        pts[y].0 += yellow;
                    }
                }
                pts.sort_by_key(|x| x.0);
                (0..num_winners).into_par_iter().map(move |i| pts[i].1)
            })
            .collect()
    }
}

impl Genome {
    fn eval_action<S1, S2>(&self, act: usize, side: Side, game: &Game<S1, S2>) -> f32 {
        let cooldowns = game.cooldowns();
        let state = game.state();
        let g = &self.0[act];
        let (us, them) = if side == Side::Blue {
            (cooldowns.blue, cooldowns.yellow)
        } else {
            (cooldowns.yellow, cooldowns.blue)
        };

        g[0] * cooldowns.tornado as f32
            + g[1] * us.ball as f32
            + g[2] * them.ball as f32
            + g[3] * us.bumper as f32
            + g[4] * them.bumper as f32
            + g[5] * us.mushroom as f32
            + g[6] * them.mushroom as f32
            + g[7] * us.special as f32
            + g[8] * them.special as f32
            + g[9] * state.speed()
            + g[10] * state.drop_speed()
            + if state.side() == side { g[11] } else { 0. }
            + g[12]
    }

    pub fn choose<S1, S2>(&self, game: &Game<S1, S2>, side: Side) -> Action {
        let mut val_max = self.eval_action(0, side, game);
        let mut act_max = Action::Nothing;
        game.valid_actions(side, |act, i| {
            let val = self.eval_action(i, side, game);
            if val > val_max {
                val_max = val;
                act_max = act;
            }
        });
        act_max
    }
}

fn eval(genome_blue: &Genome, genome_yellow: &Genome) -> (i16, i16) {
    let mut game = Game::<BlackDrop, BlackDrop>::start(Side::Blue);
    while !game.should_quit() {
        let choice_blue = genome_blue.choose(&game, Side::Blue);
        let choice_yellow = genome_yellow.choose(&game, Side::Yellow);
        game.action(Side::Blue, choice_blue);
        game.action(Side::Yellow, choice_yellow);
        game.tick();
    }
    let blue = game.state().points()[0] as i16 * 3 - game.state().points()[1] as i16;
    let yellow = game.state().points()[1] as i16 * 3 - game.state().points()[0] as i16;
    (blue, yellow)
}

impl Genome {
    fn crossover(parents: Vec<&Self>, num: usize, num_cut_points: usize) -> Vec<Self> {
        // breed one child for each partner in parents
        (0..num)
            .into_par_iter()
            .map_init(rand::thread_rng, |rng, _| {
                let mut chooser = parents.choose_multiple(rng, 2);
                let mut genome = (*chooser.next().expect("No parents")).clone();
                let partner = chooser.next().expect("No parents");
                for (k, v) in genome.0.iter_mut().enumerate() {
                    for _ in 0..num_cut_points {
                        let start = rng.gen_range(0, v.len());
                        let end = rng.gen_range(start, v.len());
                        v[start..end].copy_from_slice(&partner.0[k][start..end]);
                    }
                }
                genome
            })
            .collect()
    }
}

impl Genome {
    fn mutate<R>(&mut self, mutation_rate: f64, rng: &mut R)
    where
        R: Rng + Sized,
    {
        for v in self.0.iter_mut() {
            let num_mutations =
                ((v.len() as f64 * mutation_rate) + rng.gen::<f64>()).floor() as usize;

            for _ in 0..num_mutations {
                let index = rng.gen_range(0, v.len());
                v[index] = rng.gen_range(-10., 10.);
            }
        }
    }
}

pub fn run() -> Result<(), ()> {
    const POP: usize = 2000;
    const GENERATIONS: u64 = 20; // 120_000;

    let mut rng = ThreadRng::default();
    let mut population: Vec<Genome> = std::fs::File::open("serialized.csv")
        .ok()
        .and_then(|f| {
            csv::Reader::from_reader(f)
                .deserialize()
                .collect::<Result<Vec<Genome>, _>>()
                .ok()
        })
        .filter(|p| !p.is_empty())
        .unwrap_or_else(|| {
            (0..POP)
                .map(|_| {
                    Genome({
                        let mut out = [[0.; 13]; 6];
                        for v in &mut out {
                            for v in v {
                                *v = rng.gen_range(-10., 10.);
                            }
                        }
                        out
                    })
                })
                .collect()
        });

    let selector = TournamentSelector::new(0.7, 2, 20, 0.8);

    let mut pb = ProgressBar::new(GENERATIONS);
    for step in 0..GENERATIONS {
        population.shuffle(&mut rng);
        let selected = selector.select_from(&population, 2);
        let mut new_gen = Genome::crossover(selected, POP, 4);
        for genome in &mut new_gen {
            genome.mutate(0.005 / (step as f64).ln(), &mut rng);
        }
        population = new_gen;
        pb.inc();
    }
    pb.finish_print("Done.");
    println!("{:#?}", population[0]);
    let headers = [
        "Nothing", "Tornado", "Ball", "Bumper", "Special", "Mushroom",
    ]
    .iter()
    .flat_map(|action| {
        [
            "Tornado",
            "Us ball",
            "Them ball",
            "Us bumper",
            "Them bumper",
            "Us mushroom",
            "Them mushroom",
            "Us Special",
            "Them special",
            "Speed",
            "Drop Speed",
            "Side",
            "Base",
        ]
        .iter()
        .map(move |value| format!("{}: {}", action, value))
    });
    let mut w = csv::Writer::from_writer(std::fs::File::create("serialized.csv").unwrap());
    w.write_record(headers).unwrap();
    let selected = selector.select_from(&population, 2);
    for person in selected {
        w.serialize(person).unwrap();
    }
    Ok(())
}
