use crossterm::event::{self, Event as CEvent, KeyCode};
use crossterm::terminal::{EnterAlternateScreen, LeaveAlternateScreen};
use std::cmp::Ordering;
use std::io::{self, Write};
use std::time::Duration;
use tui::backend::CrosstermBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, List, Paragraph, Row, Sparkline, Table, Text, Widget};
use tui::Terminal;

use super::sim::Genome;
use sim::{Action, BlackDrop, Cooldowns, Game, Side};

fn row<T: Copy + Into<usize> + std::fmt::Display>(
    name: &str,
    key: &str,
    cooldown: T,
) -> Row<impl ExactSizeIterator<Item = String>, String> {
    let color = if cooldown.into() == 0 {
        Color::Green
    } else {
        Color::DarkGray
    };
    Row::StyledData(
        vec![name.to_string(), format!("{}s", cooldown), key.to_string()].into_iter(),
        Style::default().fg(color),
    )
}

fn player(cooldowns: &Cooldowns, side: Side, keys: [&'static str; 5]) -> impl Widget + 'static {
    let personal_cooldowns = if side == Side::Blue {
        &cooldowns.blue
    } else {
        &cooldowns.yellow
    };
    Table::new(
        ["Powerup", "State", "Key"].iter(),
        vec![
            // "Tornado", "Ball", "Bumper", "Mushroom", "Special"
            row("Tornado", keys[0], cooldowns.tornado),
            row("Ball", keys[1], personal_cooldowns.ball),
            row("Bumper", keys[2], personal_cooldowns.bumper),
            row("Mushroom", keys[3], personal_cooldowns.mushroom),
            row("Special", keys[4], personal_cooldowns.special),
        ]
        .into_iter(),
    )
    .block(
        Block::default()
            .title(if side == Side::Blue {
                "Joueur bleu"
            } else {
                "Joueur jaune"
            })
            .borders(Borders::ALL),
    )
    .header_style(Style::default().fg(Color::Yellow))
    .widths(&[
        Constraint::Length(10),
        Constraint::Length(7),
        Constraint::Min(5),
    ])
    .style(Style::default().fg(Color::White))
    .column_spacing(1)
}

fn draw<W: Write, S1, S2>(
    term: &mut Terminal<CrosstermBackend<W>>,
    state: &Game<S1, S2>,
    log: &[Text<'static>],
) -> io::Result<()> {
    term.draw(|mut f| {
        let v = Layout::default()
            .direction(Direction::Horizontal)
            .margin(1)
            .constraints([Constraint::Min(17 * 2), Constraint::Percentage(20)].as_ref())
            .split(f.size());
        let h = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(10), Constraint::Percentage(50)].as_ref())
            .split(v[0]);
        let desc = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
            .split(h[1]);
        let mut vals = (0..h[0].width - 2).map(|_| 0).collect::<Vec<_>>();
        let len = state.state().tray().len();
        let each = len / vals.len() as usize;
        let extra = len % vals.len() as usize;
        let mut extra_left = extra;

        let mut start = 0;
        for n in 0..vals.len() {
            let used_extra = if extra_left * vals.len() > extra * (vals.len() - n) {
                1
            } else {
                0
            };
            let end = start + each + used_extra;
            vals[n] = state.state().tray()[start..end].iter().sum::<u8>() as u64;
            extra_left -= used_extra;
            start = end;
        }

        Sparkline::default()
            .block(Block::default().title("Tray").borders(Borders::ALL))
            .max(10)
            .data(&vals)
            .render(&mut f, h[0]);
        List::new(log.iter().cloned())
            .block(Block::default().title("Actions").borders(Borders::ALL))
            .render(&mut f, v[1]);
        let text = [
            ("Time left: ", format!("{}s\n", state.round())),
            ("Speed: ", format!("{}\n", state.state().speed())),
            ("Drop speed: ", format!("{}\n", state.state().drop_speed())),
            ("Reset: ", format!("{}s\n", state.cooldowns().reset)),
            (
                "Score: ",
                format!(
                    "blue: {}, yellow: {}\n",
                    state.state().points()[0],
                    state.state().points()[1]
                ),
            ),
        ];
        let styles: Vec<_> = text
            .iter()
            .flat_map(|(title, value)| {
                vec![
                    Text::styled(
                        *title,
                        Style::default().fg(Color::Red).modifier(Modifier::BOLD),
                    ),
                    Text::raw(value),
                ]
            })
            .collect();
        Paragraph::new(styles.iter())
            .block(
                Block::default()
                    .borders(Borders::ALL)
                    .border_style(Style::default().fg(if state.state().side() == Side::Blue {
                        Color::Blue
                    } else {
                        Color::Yellow
                    })),
            )
            .wrap(true)
            .render(&mut f, h[0]);

        // "Tornado", "Ball", "Bumper", "Mushroom", "Special"
        player(
            state.cooldowns(),
            Side::Blue,
            ["Left", "Up", "Right", "Down", "Space"],
        )
        .render(&mut f, desc[0]);
        player(state.cooldowns(), Side::Yellow, ["a", "w", "d", "s", "e"]).render(&mut f, desc[1]);
    })
}

fn add_log(log: &mut Vec<Text<'static>>, text: Action, side: Side, round: u16) {
    log.push(Text::styled(
        format!("r {}: {:?}", 300 - round, text),
        Style::default().fg(if side == Side::Blue {
            Color::Blue
        } else {
            Color::Yellow
        }),
    ));
}

#[derive(Clone, Debug)]
pub enum Player {
    Human,
    Bot(Genome),
}

fn blue_action<S1, S2>(key: KeyCode, game: &Game<S1, S2>) -> Option<Action> {
    match key {
        // "Tornado", "Ball", "Bumper", "Mushroom", "Special"
        KeyCode::Left if game.cooldowns().tornado == 0 => Some(Action::Tornado),
        KeyCode::Up if game.cooldowns().blue.ball == 0 => Some(Action::Ball),
        KeyCode::Right if game.cooldowns().blue.bumper == 0 => Some(Action::Bumper),
        KeyCode::Down if game.cooldowns().blue.mushroom == 0 => Some(Action::MushroomFar),
        KeyCode::Char(' ') if game.cooldowns().blue.special == 0 => Some(Action::Special),
        _ => None,
    }
}

fn yellow_action<S1, S2>(key: KeyCode, game: &Game<S1, S2>) -> Option<Action> {
    match key {
        // "Tornado", "Ball", "Bumper", "Mushroom", "Special"
        KeyCode::Char('a') if game.cooldowns().tornado == 0 => Some(Action::Tornado),
        KeyCode::Char('w') if game.cooldowns().yellow.ball == 0 => Some(Action::Ball),
        KeyCode::Char('d') if game.cooldowns().yellow.bumper == 0 => Some(Action::Bumper),
        KeyCode::Char('s') if game.cooldowns().yellow.mushroom == 0 => Some(Action::MushroomFar),
        KeyCode::Char('e') if game.cooldowns().yellow.special == 0 => Some(Action::Special),
        _ => None,
    }
}

pub fn run(blue: Player, yellow: Player) -> Result<(), Box<dyn std::error::Error>> {
    crossterm::terminal::enable_raw_mode()?;

    let mut stdout = std::io::stdout();
    crossterm::execute!(stdout, EnterAlternateScreen)?;

    let backend = CrosstermBackend::new(stdout);

    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;
    terminal.clear()?;

    let mut game = Game::<BlackDrop, BlackDrop>::start(Side::Yellow);
    let mut log = Vec::with_capacity(60);

    'gameloop: while !game.should_quit() {
        for _ in 0..50 {
            draw(&mut terminal, &game, &log)?;
            let game_bis = game.clone();
            if let Player::Bot(genome) = &blue {
                let action = genome.choose(&game_bis, Side::Blue);
                if action != Action::Nothing {
                    add_log(&mut log, action, Side::Blue, game.round());
                    game.action(Side::Blue, action)
                }
            }
            if let Player::Bot(genome) = &yellow {
                let action = genome.choose(&game_bis, Side::Yellow);
                if action != Action::Nothing {
                    add_log(&mut log, action, Side::Yellow, game.round());
                    game.action(Side::Yellow, action)
                }
            }
            std::thread::sleep(Duration::from_millis(10));
            // poll for tick rate duration, if no events, sent tick event.
            while event::poll(Duration::from_secs(0)).unwrap() {
                if let CEvent::Key(event) = event::read().unwrap() {
                    if event.code == KeyCode::Char('q') {
                        break 'gameloop;
                    }
                    if let Player::Human = &blue {
                        if let Some(action) = blue_action(event.code, &game_bis) {
                            add_log(&mut log, action, Side::Blue, game.round());
                            game.action(Side::Blue, action)
                        }
                    }
                    if let Player::Human = &yellow {
                        if let Some(action) = yellow_action(event.code, &game_bis) {
                            add_log(&mut log, action, Side::Yellow, game.round());
                            game.action(Side::Yellow, action)
                        }
                    }
                }
            }
        }
        game.tick();
    }
    crossterm::terminal::disable_raw_mode()?;
    crossterm::execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
    terminal.show_cursor()?;
    let pts = game.state().points();
    match pts[0].cmp(&pts[1]) {
        Ordering::Equal => println!("Tie, both teams made {} pts", pts[0]),
        Ordering::Greater => println!("Blue won {} to {}", pts[0], pts[1]),
        Ordering::Less => println!("Yellow won {} to {}", pts[1], pts[0]),
    }

    Ok(())
}
