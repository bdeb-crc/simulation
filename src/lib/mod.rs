use enum_map::Enum;
use serde::{Deserialize, Serialize};

mod cooldowns;
mod state;

#[derive(Debug, Clone, Hash, PartialEq, Eq, Copy)]
pub enum Side {
    Blue,
    Yellow,
}

pub use cooldowns::Cooldowns;
pub use state::GameState;

use state::{DropSpeed, Speed};
use std::marker::PhantomData;

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Game<S1, S2> {
    state: GameState,
    cooldowns: Cooldowns,
    tick: u16,
    special_blue: PhantomData<S1>,
    special_yellow: PhantomData<S2>,
}

#[derive(Debug, Clone, PartialEq, Eq, Copy, Enum, Serialize, Deserialize)]
pub enum Action {
    MushroomFar,
    MushroomNear,
    Ball,
    Bumper,
    Special,
    Tornado,
    Nothing,
}

#[derive(Debug, Clone, Copy)]
pub struct SlowConveyor;
#[derive(Debug, Clone, Copy)]
pub struct FastDrop;
#[derive(Debug, Clone, Copy)]
pub struct BlackDrop;

pub trait Special: Copy {
    fn special(state: &mut GameState);

    fn cooldown() -> u8;
}

impl Special for SlowConveyor {
    fn special(state: &mut GameState) {
        state.set_speed(Speed::Half);
    }

    fn cooldown() -> u8 {
        20
    }
}

impl Special for FastDrop {
    fn special(state: &mut GameState) {
        state.set_drop_speed(DropSpeed::Triple);
    }

    fn cooldown() -> u8 {
        20
    }
}

impl Special for BlackDrop {
    fn special(state: &mut GameState) {
        state.drop_beans(20);
    }

    fn cooldown() -> u8 {
        255
    }
}

impl<S1, S2> Game<S1, S2> {
    pub fn state(&self) -> &GameState {
        &self.state
    }

    pub fn cooldowns(&self) -> &Cooldowns {
        &self.cooldowns
    }

    pub fn round(&self) -> u16 {
        self.tick
    }

    pub fn should_quit(&self) -> bool {
        self.tick == 0
    }

    pub fn valid_actions<F: FnMut(Action, usize)>(&self, side: Side, mut f: F) {
        let cooldowns = self.cooldowns();
        if cooldowns.tornado == 0 {
            f(Action::Tornado, 1);
        }
        let personal_cooldowns = if side == Side::Blue {
            &cooldowns.blue
        } else {
            &cooldowns.yellow
        };
        if personal_cooldowns.ball == 0 {
            f(Action::Ball, 2);
        }
        if personal_cooldowns.bumper == 0 {
            f(Action::Bumper, 3);
        }
        if personal_cooldowns.special == 0 {
            f(Action::Special, 4);
        }
        if personal_cooldowns.mushroom == 0 {
            f(Action::MushroomFar, 5);
        }
    }
}

impl<S1: Special, S2: Special> Game<S1, S2> {
    pub fn start(side: Side) -> Self {
        Self {
            state: GameState::start(side),
            cooldowns: Cooldowns::default(),
            tick: 300,
            special_blue: PhantomData,
            special_yellow: PhantomData,
        }
    }

    pub fn action(&mut self, side: Side, action: Action) {
        match action {
            Action::MushroomFar => {
                self.state.set_speed(Speed::Double);
                self.cooldowns.reset = 20;
                match side {
                    Side::Blue => self.cooldowns.blue.mushroom = 20,
                    Side::Yellow => self.cooldowns.yellow.mushroom = 20,
                }
            }
            Action::MushroomNear => {
                self.state.set_speed(Speed::ThreeHalf);
                self.cooldowns.reset = 20;
                match side {
                    Side::Blue => self.cooldowns.blue.mushroom = 20,
                    Side::Yellow => self.cooldowns.yellow.mushroom = 20,
                }
            }
            Action::Ball => {
                self.state.set_drop_speed(DropSpeed::Double);
                self.cooldowns.reset = 20;
                match side {
                    Side::Blue => self.cooldowns.blue.ball = 20,
                    Side::Yellow => self.cooldowns.yellow.ball = 20,
                }
            }
            Action::Tornado => {
                self.cooldowns.reset = 0;
                self.cooldowns.tornado = 10;
                let speed = if self.state.stopped() {
                    Speed::Normal
                } else {
                    Speed::Stopped
                };
                self.state.set_speed(speed);
            }
            Action::Bumper => {
                self.state.switch();
                self.cooldowns.reset = 241;
                match side {
                    Side::Blue => self.cooldowns.blue.bumper = 241,
                    Side::Yellow => self.cooldowns.yellow.bumper = 241,
                }
            }
            Action::Special => match side {
                Side::Blue => {
                    S1::special(&mut self.state);
                    self.cooldowns.blue.special = S1::cooldown();
                    self.cooldowns.reset = self.cooldowns.blue.special;
                }
                Side::Yellow => {
                    S2::special(&mut self.state);
                    self.cooldowns.yellow.special = S2::cooldown();
                    self.cooldowns.reset = self.cooldowns.yellow.special;
                }
            },
            Action::Nothing => (),
        }
    }

    pub fn tick(&mut self) {
        self.state.tick();
        self.tick -= 1;
        self.cooldowns.update();
        if self.cooldowns.reset == 1 {
            self.state.reset();
        }
        self.cooldowns.reset = self.cooldowns.reset.saturating_sub(1);
    }
}
