use super::Side;

#[derive(Debug, Clone, Hash, PartialEq, Eq, Copy)]
pub enum Speed {
    Stopped,
    Half,
    Normal,
    ThreeHalf,
    Double,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Copy)]
pub enum DropSpeed {
    Half,
    Normal,
    Double,
    Triple,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct GameState {
    tray: Vec<u8>,
    speed: Speed,
    drop_speed: DropSpeed,
    drop_ticks: u8,
    side: Side,
    side_ticks: i8,
    points: [u8; 2],
}

impl GameState {
    const SIDE_SWITCH_TICKS: i8 = 30;

    fn start_with_len(side: Side, len: usize) -> Self {
        Self {
            tray: vec![0; 2 * len + 1],
            speed: Speed::Normal,
            drop_speed: DropSpeed::Normal,
            drop_ticks: 0,
            side,
            side_ticks: -Self::SIDE_SWITCH_TICKS,
            points: [0; 2],
        }
    }

    pub fn start(side: Side) -> Self {
        const TRAY_LEN: usize = 40; // 40s from one side to the other
        Self::start_with_len(side, TRAY_LEN)
    }

    pub fn reset(&mut self) {
        self.speed = Speed::Normal;
        self.drop_speed = DropSpeed::Normal;
    }

    pub fn tray(&self) -> &Vec<u8> {
        &self.tray
    }

    pub fn points(&self) -> [u8; 2] {
        self.points
    }

    pub fn switch(&mut self) {
        self.side_ticks = 0;
        self.side = if self.side == Side::Blue {
            Side::Yellow
        } else {
            Side::Blue
        };
    }

    pub fn stopped(&self) -> bool {
        self.speed == Speed::Stopped
    }

    pub fn speed(&self) -> f32 {
        match self.speed {
            Speed::Stopped => 0.,
            Speed::Half => 0.5,
            Speed::Normal => 1.,
            Speed::ThreeHalf => 1.5,
            Speed::Double => 2.,
        }
    }

    pub fn set_speed(&mut self, speed: Speed) {
        self.speed = speed;
    }

    pub fn drop_speed(&self) -> f32 {
        match self.drop_speed {
            DropSpeed::Half => 0.5,
            DropSpeed::Normal => 1.,
            DropSpeed::Double => 2.,
            DropSpeed::Triple => 3.,
        }
    }

    pub fn set_drop_speed(&mut self, drop_speed: DropSpeed) {
        self.drop_speed = drop_speed;
    }

    pub fn drop_beans(&mut self, num: u8) {
        let idx = (self.tray.len() - 1) / 2;
        self.tray[idx] += num;
    }

    pub fn side(&self) -> Side {
        self.side
    }

    pub fn tick(&mut self) {
        let drift = match self.speed {
            Speed::Stopped => 0,
            Speed::Half => 1,
            Speed::Normal => 2,
            Speed::ThreeHalf => 3,
            Speed::Double => 4,
        };
        self.side_ticks += 1;
        if self.side_ticks >= Self::SIDE_SWITCH_TICKS {
            self.side_ticks = 0;
            self.side = if self.side == Side::Blue {
                Side::Yellow
            } else {
                Side::Blue
            };
        }

        let drop_speed = match self.drop_speed {
            DropSpeed::Triple => 1,
            DropSpeed::Double => 2,
            DropSpeed::Normal => 4,
            DropSpeed::Half => 8,
        };
        let drift_end = self.tray.len() - drift;
        let mid = (self.tray.len() - 1) / 2;
        match self.side {
            Side::Blue => {
                self.points[0] += self.tray[..drift].iter().cloned().sum::<u8>();
                self.tray.copy_within(drift.., 0);
                for v in &mut self.tray[drift_end..] {
                    *v = 0;
                }
                self.drop_ticks += 1;
                if self.drop_ticks >= drop_speed {
                    self.drop_ticks = 0;
                    self.tray[mid] += 1;
                }
            }
            Side::Yellow => {
                self.points[1] += self.tray[self.tray.len() - drift..]
                    .iter()
                    .cloned()
                    .sum::<u8>();
                self.tray.copy_within(..drift_end, drift);
                for v in &mut self.tray[..drift] {
                    *v = 0;
                }
                self.drop_ticks += 1;
                if self.drop_ticks >= drop_speed {
                    self.drop_ticks = 0;
                    self.tray[mid] += 1;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn tick_test(mut state: GameState, drop_time: usize, speed: usize) {
        let mut ticks: usize = 0;
        for j in 0..(30 / drop_time) {
            for i in 0..drop_time {
                assert_eq!(state.side_ticks, ticks as u8 % GameState::SIDE_SWITCH_TICKS);
                let mut tray = [0; 17];
                for k in 0..j {
                    if let Some(v) = 8_usize
                        .checked_sub(speed * (k * drop_time + i as usize))
                        .and_then(|i| tray.get_mut(i))
                    {
                        *v = 1;
                    }
                }
                assert_eq!(state.tray, tray, "failed at tick: ({}, {})", i, j);
                assert_eq!(state.side, Side::Blue);
                assert_eq!(
                    state.points,
                    [(ticks.saturating_sub(1 + 8 / speed) / drop_time) as u8, 0],
                    "failed at round: ({}, {})",
                    ticks,
                    j
                );
                ticks += 1;
                state.tick();
            }
        }
    }

    #[test]
    fn tick() {
        let state = GameState::start_with_len(Side::Blue, 8);
        tick_test(state, 4, 2);
    }

    #[test]
    fn tick_double_speed() {
        let mut state = GameState::start_with_len(Side::Blue, 8);
        state.speed = Speed::Double;
        tick_test(state, 4, 4);
    }

    #[test]
    fn tick_double_speed_drop() {
        let mut state = GameState::start_with_len(Side::Blue, 8);
        state.drop_speed = DropSpeed::Double;
        tick_test(state, 2, 2);
    }
}
