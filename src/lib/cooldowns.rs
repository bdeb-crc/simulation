#[derive(Default, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Cooldowns {
    pub blue: SideCooldowns,
    pub yellow: SideCooldowns,
    pub tornado: u8,
    pub reset: u8,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Copy)]
pub struct SideCooldowns {
    pub mushroom: u8,
    pub ball: u8,
    pub bumper: u8,
    pub special: u8,
}

impl Default for SideCooldowns {
    fn default() -> Self {
        Self {
            mushroom: 0,
            ball: 0,
            bumper: 60,
            special: 0,
        }
    }
}

impl Cooldowns {
    pub fn update(&mut self) {
        self.tornado = self.tornado.saturating_sub(1);
        self.blue.update();
        self.yellow.update();
    }
}

impl SideCooldowns {
    pub fn update(&mut self) {
        self.mushroom = self.mushroom.saturating_sub(1);
        self.ball = self.ball.saturating_sub(1);
        self.bumper = self.bumper.saturating_sub(1);
        if self.special != 255 {
            self.special = self.special.saturating_sub(1);
        }
    }
}
